//
//  InstallationScreenViewController.swift
//  ChrisMomChrilChild
//
//  Created by Jeyavijay on 30/06/16.
//

import UIKit

class InstallationScreenViewController: UIViewController ,UIScrollViewDelegate{

    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)

    @IBOutlet var buttonBack: UIButton!
    @IBOutlet var textViewRules: UITextView!

    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let stringRules:NSString = "This App allows you to play the Chris mom Chris child game via Email with all your loved ones even if there are far away from you this Christmas.\n\nRules of the game:\n\n 1. All the participants who are willing to contribute in this game have to give their name.\n\n2. All the names will be written on a different paper and mixed up. ( Instead you can use this App to create a group and enter all name and email id of the participants ).\n\n 3. Everyone has to pick one paper and the participant who took the paper is the Chris Mom for the person whose name was in the paper. ( The App randomly picks a Chris child for everyone in the group and send's an email to Chris mom’s ).\n\n4. The main thing is that no one should disclose the name they got.\n\n5. The Chris Mom should send gifts to his/her child indirectly and the child should try to find their mom.\n\n6. It's not compulsory to send gifts daily but should give as much as the mom can.\n\n7. The surprise would be announced to the child on 25th Dec and the Mom should give a memorable gift to the child. We hope that everyone will like this game and so we play the game and celebrate this Christmas with more joy.\n\n8. On Christmas day this App will show the details of the Chris child’s."
        let attributedText: NSMutableAttributedString = NSMutableAttributedString(string: stringRules as String)
        attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location:0,length:1226))
        attributedText.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 16)], range: NSRange(location: 0, length: 1226))

        attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location:329,length:104))
        
        attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location:569,length:101))
        
        attributedText.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)], range: NSRange(location: 329, length: 104))
        
        attributedText.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)], range: NSRange(location: 569, length: 101))

        
        attributedText.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)], range: NSRange(location: 0, length: 147))
        attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location:0,length:147))

        attributedText.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)], range: NSRange(location: 147, length: 18))
        attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location:147,length:18))

        textViewRules.attributedText = attributedText

    }
    func applyPlainShadow(_ view: UIView) {
        let layer = view.layer
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 5
        layer.cornerRadius = 4
    }
    

    
    @IBAction func buttonBackSearch(_ sender: AnyObject)
    {
      //  let nextViewController = storyBoard.instantiateViewController(withIdentifier:"LoginPage") as! LoginPage
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    
}
