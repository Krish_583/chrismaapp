//
//  LoginPage.swift
//  ChrisMomChrilChild
//
//  Created by Jeyavijay on 07/07/16.
//  Copyright © 2016 Jeyavijay. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

class LoginPage: UIViewController
{
    @IBOutlet var buttonRules: UIButton!
    @IBOutlet var buttonFAQ: UIButton!
    @IBOutlet var buttonPlayGame: UIButton!
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    let parameter = NSMutableDictionary()
    
    @IBOutlet weak var labelChristmasText: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        applyPlainShadow(buttonRules)
        applyPlainShadow(buttonPlayGame)
        applyPlainShadow(buttonFAQ)
        labelChristmasText.text = "Merry Christmas\nand\nHappy NewYear"

    }
    func applyPlainShadow(_ view: UIView) {
        let layer = view.layer
        
        layer.shadowColor = UIColor.red.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 5
        layer.cornerRadius = 4
    }
    @IBAction func buttonRules(_ sender:UIButton)
    {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:"InstallationScreenViewController") as! InstallationScreenViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func buttonFAQ(_ sender:UIButton)
    {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:"EditContactViewController") as! EditContactViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func buttonPlay(_ sender:UIButton)
    {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:"GroupNameViewController") as! GroupNameViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    

}
