//
//  GroupNameTableViewCell.swift
//  ChrisMomChrilChild
//
//  Created by Jeyavijay on 27/07/16.
//  Copyright © 2016 Jeyavijay. All rights reserved.
//

import UIKit

class GroupNameTableViewCell: UITableViewCell {

    @IBOutlet var imageviewBackground: UIImageView!

    @IBOutlet var labelGroupName: UILabel!
    @IBOutlet var buttonPlay: UIButton!
    @IBOutlet var buttonEdit: UIButton!
    @IBOutlet var buttonDelete: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
