//
//  ViewController.swift
//  CMomCChild
//
//  Created by Jeyavijay on 02/11/16.
//  Copyright © 2016 Jeyavijay. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableViewResults: UITableView!
    @IBOutlet weak var labelID: UILabel!
    let SQLITE_STATIC = unsafeBitCast(0, to: sqlite3_destructor_type.self)
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)

    var sDBID = NSString()
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    var  arrayResultChrisMom = NSMutableArray()
    var  arrayResultChrisChild = NSMutableArray()
    var  arrayResultChrisChildsData = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableViewResults.dataSource = self
        tableViewResults.delegate = self
        self.tableViewResults.register(UINib(nibName: "ResultPageTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "Result")
        selectFromDB()
        selectFromResultDB()
        playAgain()

    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrayResultChrisChild.count
    }

    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let viewHeader = UIView()
        viewHeader.backgroundColor = UIColor.clear
        return viewHeader
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Result") as! ResultPageTableViewCell!

        
        
        cell?.labelChrisMom.text = NSString(format: "%@", ((arrayResultChrisMom.object(at: (indexPath as NSIndexPath).section) as AnyObject).value(forKey: "Name") as? String)!) as String
        cell?.labelChrisChild.text = NSString(format: "%@", ((arrayResultChrisChildsData.object(at: (indexPath as NSIndexPath).section) as AnyObject).value(forKey: "Name") as? String)!) as String

        return cell!
        
    }

    func selectFromDB()
    {
        arrayResultChrisMom.removeAllObjects()
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ChrisMomChrisSon.sqlite")
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        // Select Values From DB
        var statement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, "select * from GroupContacts where gid= (?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        let cGroupID = (sDBID as NSString).utf8String

        if sqlite3_bind_text(statement, 1, cGroupID, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        while sqlite3_step(statement) == SQLITE_ROW
        {
            let dTempDictionary = NSMutableDictionary()
            let name = sqlite3_column_text(statement, 1)
            if name != nil
            {
                let nameString = String(cString:(name!))
                print(nameString)
                dTempDictionary.setValue(nameString, forKey: "Name")
            }
            let email = sqlite3_column_text(statement, 2)
            if email != nil
            {
                let emailString = String(cString:(email!))
                print(emailString)
                dTempDictionary.setValue(emailString, forKey: "Email")
            }
            let idPrimaryKey = sqlite3_column_text(statement, 3)
            if idPrimaryKey != nil
            {
                let idPrimaryKey = String(cString:(idPrimaryKey!))
                print(idPrimaryKey)
                dTempDictionary.setValue(idPrimaryKey, forKey: "PrimaryKey")
            }
            arrayResultChrisMom.add(dTempDictionary)
            print(arrayResultChrisMom)
            var set = NSSet()
            set = NSSet(array: arrayResultChrisMom as [AnyObject] )
            arrayResultChrisMom = NSMutableArray(array: set.allObjects)
            print(arrayResultChrisMom)
            let   brandDescriptor = NSSortDescriptor(key: "Name", ascending: true)
            let sortDescriptors = [brandDescriptor]
            let sortedArray = (arrayResultChrisMom as NSMutableArray).sortedArray(using: sortDescriptors)
            print(sortedArray)
            arrayResultChrisMom.removeAllObjects()
            arrayResultChrisMom.addObjects(from: sortedArray)
            
        }
        print(arrayResultChrisMom.count)
        
       // insertGroupCount()
        tableViewResults.reloadData()
    }
    func selectFromResultDB()
    {
        arrayResultChrisChild.removeAllObjects()
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ChrisMomChrisSon.sqlite")
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK
        {
            print("error opening database")
        }
        // Select Values From DB
        let cGroupID = (sDBID as NSString).utf8String
        var statement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, "select * from resultTable where gid= (?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 1, cGroupID, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        while sqlite3_step(statement) == SQLITE_ROW
        {
            let dTempDictionary = NSMutableDictionary()
            let mom_id = sqlite3_column_text(statement, 1)
            if mom_id != nil
            {
                let momIDString = String(cString:(mom_id!))
                print(momIDString)
                dTempDictionary.setValue(momIDString, forKey: "mom_id")
            }
            let childID = sqlite3_column_text(statement, 2)
            if childID != nil
            {
                let childIDString = String(cString:(childID!))
                print(childIDString)
                dTempDictionary.setValue(childIDString, forKey: "child_id")
            }
            let idPrimaryKey = sqlite3_column_text(statement, 3)
            if idPrimaryKey != nil
            {
                let idPrimaryKey = String(cString:(idPrimaryKey!))
                print(idPrimaryKey)
                dTempDictionary.setValue(idPrimaryKey, forKey: "PrimaryKey")
            }
            arrayResultChrisChild.add(dTempDictionary)

        }
        print(arrayResultChrisChild)
    }

    func playAgain()
    {
        arrayResultChrisChildsData.removeAllObjects()
        print(arrayResultChrisChild.count)
        print(arrayResultChrisMom.count)
        
        for i in 0...arrayResultChrisChild.count-1
        {
            let parameter = NSMutableDictionary()
            let nShuffledName:NSString = NSString(format:"%@",((arrayResultChrisChild.object(at: i) as AnyObject).value(forKey: "child_id") as? String)!)
            var nChildValue:Int = Int(nShuffledName.intValue)
            nChildValue = nChildValue - 1
            print(nChildValue)
            var stringShuffledName = NSString()
            stringShuffledName = NSString(format: "%@", ((arrayResultChrisMom.object(at: nChildValue as Int ) as AnyObject).value(forKey: "Name") as? String)!)
            parameter.setValue(stringShuffledName, forKey: "Name")
            arrayResultChrisChildsData.add(parameter)
            print(arrayResultChrisChildsData.count)
            
        }
        print(arrayResultChrisChildsData)
        tableViewResults.reloadData()
    }


}

