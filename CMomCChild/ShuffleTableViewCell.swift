//
//  ShuffleTableViewCell.swift
//  ChrisMomChrilChild
//
//  Created by Jeyavijay on 27/06/16.
//  Copyright © 2016 Jeyavijay. All rights reserved.
//

import UIKit

class ShuffleTableViewCell: UITableViewCell
{
    
    @IBOutlet var buttonShuffle : UIButton!
    @IBOutlet var buttonReshuffle : UIButton!
    
    @IBOutlet var labelChrisMom : UILabel!
    @IBOutlet var labelChrisChild : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
