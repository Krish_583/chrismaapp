
import UIKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

class GroupNameViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {

    let SQLITE_STATIC = unsafeBitCast(0, to: sqlite3_destructor_type.self)
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var stringDeleteGroupName = NSString()
    //
    var newarry = NSArray()
    var  arrayGroupName = NSMutableArray()
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)

    @IBOutlet var labelNoOfGroups: UILabel!
    var bgroupName = Bool()
    @IBOutlet var labelNoGroupsAvailable: UILabel!
    @IBOutlet var viewFroup: UIView!
    @IBOutlet var buttonNewGroup: UIButton!
    
    @IBOutlet var textGroupName: UITextField!
    @IBOutlet var tableViewGroupName: UITableView!
    var tap = UITapGestureRecognizer()

  
    override func viewDidLoad()
    {
        super.viewDidLoad()
        viewFroup.isHidden = true
        tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        textGroupName.attributedPlaceholder = NSAttributedString(string: " Group Name", attributes: [NSForegroundColorAttributeName: UIColor.white])

        buttonNewGroup.layer.borderColor = UIColor.green.cgColor
        tableViewGroupName.dataSource = self
        tableViewGroupName.delegate = self
        labelNoOfGroups.isHidden = true
        self.tableViewGroupName.register(UINib(nibName: "GroupNameTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "Cell")
        
       selectFromDB()

        if arrayGroupName.count == 0
        {
            labelNoGroupsAvailable.isHidden = false
            viewFroup.isHidden = true
        }
        else
        {
            print(arrayGroupName)

            labelNoGroupsAvailable.isHidden = true
            tableViewGroupName.reloadData()

        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if string.characters.count == 0
        {
            return true
        }
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        switch textField {
        case textGroupName:
            return prospectiveText.doesNotContainCharactersIn("!@#$%^&*()_+=-<>?/:;'|[]{}") &&
                prospectiveText.characters.count <= 30
            
      
        default:
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        textGroupName .resignFirstResponder()

        return false
    }
    
    func dismissKeyboard()
    {

        textGroupName .resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        selectFromDB()
    }

    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrayGroupName.count
    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let viewHeader = UIView()
        viewHeader.backgroundColor = UIColor.clear
        return viewHeader
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! GroupNameTableViewCell!
        
        
        if (((arrayGroupName.object(at: (indexPath as NSIndexPath).section)) as AnyObject).value(forKey: "GroupCount") as? String) != nil
        {
            cell?.labelGroupName.text = NSString(format: "%@  (%@)", (((arrayGroupName.object(at: (indexPath as NSIndexPath).section)) as AnyObject).value(forKey: "GroupName") as? String)!,(((arrayGroupName.object(at: (indexPath as NSIndexPath).section)) as AnyObject).value(forKey: "GroupCount") as? String)!) as String
        }
        else
        {
            cell?.labelGroupName.text = NSString(format: "%@", (((arrayGroupName.object(at: (indexPath as NSIndexPath).section)) as AnyObject).value(forKey: "GroupName") as? String)!) as String
        }
        
        if (((arrayGroupName.object(at: (indexPath as NSIndexPath).section)) as AnyObject).value(forKey: "playstatus") as? String) != nil
        {
            let image:UIImage = UIImage(named: "imagePlyed")!
            cell?.imageviewBackground.image = image
        }
        else
        {
            let image:UIImage = UIImage(named: "plainButton")!
            cell?.imageviewBackground.image = image

        }
        

        labelNoOfGroups.isHidden = true
        labelNoOfGroups.text = NSString(format: "%d groups available", arrayGroupName.count) as String
          cell?.buttonPlay.isHidden = true
      //  cell?.buttonPlay.tag = indexPath.section
      //  [cell?.buttonPlay.addTarget(self, action:#selector(self.buttonPlay(_:)), for:UIControlEvents.touchUpInside)]
        cell?.buttonEdit.isHidden = true
        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let cell = tableView.cellForRow(at: indexPath)
        cell!.contentView.layer.borderColor = UIColor.red.cgColor
        var stringID = NSString()
        var sGroupName = NSString()

        stringID = NSString(format: "%@", (((arrayGroupName.object(at: (indexPath as NSIndexPath).section)) as AnyObject).value(forKey: "gid") as? String)!)
        sGroupName = NSString(format: "%@", (((arrayGroupName.object(at: (indexPath as NSIndexPath).section)) as AnyObject).value(forKey: "GroupName") as? String)!)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewControllerSample") as! ViewControllerSample
        nextViewController.sDBID = stringID
        nextViewController.sGroupName = sGroupName
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            print(((indexPath as NSIndexPath).section))
            stringDeleteGroupName =  NSString(format: "%@", (((arrayGroupName.object(at: (indexPath as NSIndexPath).section)) as AnyObject).value(forKey: "GroupName") as? String)!)
            print(stringDeleteGroupName)
            labelNoOfGroups.text = stringDeleteGroupName as String
            deleteFromDB()
            selectFromDB()
            view.removeGestureRecognizer(tap)
          //  arrayGroupName.removeObject(at: (indexPath as NSIndexPath).section)
            self.tableViewGroupName.reloadData()
        }

    }

    @IBAction func buttonAddGroup(_ sender: UIButton)
    {
        viewFroup.isHidden = false
        view.addGestureRecognizer(tap)
        buttonNewGroup.isHidden = true
    }

    
    func validateGroupName()
    {
        for i in 0...arrayGroupName.count-1
        {
            var stringText:NSString = NSString(format: "%@", textGroupName.text!)
            print(stringText)
            stringText = stringText.lowercased as NSString
            stringText = stringText.trimmingCharacters(in: .whitespaces) as NSString

            
            var sCheckString:NSString = NSString(format: "%@", (((arrayGroupName.object(at: i)) as AnyObject).value(forKey: "GroupName") as? String)!)
            sCheckString = sCheckString.lowercased as NSString
            sCheckString = sCheckString.trimmingCharacters(in: .whitespaces) as NSString


            
            if ((stringText as String) == (sCheckString as String))
            {
                viewFroup.isHidden = true
                labelNoGroupsAvailable.isHidden = true
                textGroupName.text = ""
                bgroupName = false

                let alertController = UIAlertController(title: "Sorry", message: "Group name already exist", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)

                break
            }
        }
    }
    
    
    @IBAction func buttonBack(_ sender: AnyObject)
    {
        buttonNewGroup.isHidden = false
        if viewFroup.isHidden == false
        {
            view.removeGestureRecognizer(tap)
            viewFroup.isHidden = true
            buttonNewGroup.isHidden = false

        }
        else if viewFroup.isHidden == true
        {
            _ = navigationController?.popViewController(animated: true)
            
        }
        
    }
    @IBAction func buttonCreateGroup(_ sender: UIButton)
    {
        bgroupName = true
        view.removeGestureRecognizer(tap)
        if textGroupName.text?.characters.count > 1
        {
            if arrayGroupName.count > 0
            {
                validateGroupName()
            }
            if (bgroupName == false)
            {
                
            }else
            {
                insertToDB()
                selectFromDB()
                print(arrayGroupName)
                viewFroup.isHidden = true
                labelNoGroupsAvailable.isHidden = true
                textGroupName.text = ""
                tableViewGroupName.reloadData()
                labelNoGroupsAvailable.isHidden = true
            }
        }
        buttonNewGroup.isHidden = false

    }
    
    func insertToDB()
    {
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ChrisMomChrisSon.sqlite")
        
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        // Insert Values to DB
        var statement: OpaquePointer? = nil

        if sqlite3_prepare_v2(db, "insert into Groups (gname) values (?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing insert: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 1, textGroupName.text, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_step(statement) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure inserting foo: \(errmsg)")
        }
        

    }
    func selectFromDB()
    {
        arrayGroupName.removeAllObjects()
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ChrisMomChrisSon.sqlite")
        
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        // Select Values From DB
        var statement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, "select * from Groups", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        while sqlite3_step(statement) == SQLITE_ROW {
            let dTempDictionary = NSMutableDictionary()
            let name = sqlite3_column_text(statement, 1)
            if name != nil {
                let nameString = String(cString:(name!))
                print(nameString)
                dTempDictionary.setValue(nameString, forKey: "GroupName")
            }
            let gCount = sqlite3_column_text(statement, 3)
            if gCount != nil {
                let gCount = String(cString:(gCount!))
                print(gCount)
                dTempDictionary.setValue(gCount, forKey: "GroupCount")
            }
            let gid = sqlite3_column_text(statement, 0)
            if gid != nil {
                let gid = String(cString:(gid!))
                print(gid)
                dTempDictionary.setValue(gid, forKey: "gid")
            }

            let playstatus = sqlite3_column_text(statement, 2)
            if playstatus != nil {
                let playstatus = String(cString:(playstatus!))
                print(playstatus)
                dTempDictionary.setValue(playstatus, forKey: "playstatus")
            }

            arrayGroupName.add(dTempDictionary)
            print(arrayGroupName)
            var set = NSSet()
            set = NSSet(array: arrayGroupName as [AnyObject] )
            arrayGroupName = NSMutableArray(array: set.allObjects)
            
        }
        let   brandDescriptor = NSSortDescriptor(key: "gid", ascending: false)
        let sortDescriptors = [brandDescriptor]
        let sortedArray = (arrayGroupName as NSMutableArray).sortedArray(using: sortDescriptors)
        print(sortedArray)
        arrayGroupName.removeAllObjects()
        arrayGroupName.addObjects(from: sortedArray)
        tableViewGroupName.reloadData()
    }
    
    func deleteFromDB()
    {
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ChrisMomChrisSon.sqlite")
        
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        // Insert Values to DB
        var statement: OpaquePointer? = nil
        
        
        if sqlite3_prepare_v2(db, "DELETE FROM Groups where gname= (?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing Delete: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 1, labelNoOfGroups.text, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_step(statement) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure deleting data: \(errmsg)")
        }
    }

}


