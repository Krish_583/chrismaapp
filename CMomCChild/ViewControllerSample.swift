import UIKit
import GameplayKit
import AVFoundation
import AFNetworking
import Contacts
import ContactsUI

class ViewControllerSample: UIViewController, EPPickerDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    // Sqlite
    let SQLITE_STATIC = unsafeBitCast(0, to: sqlite3_destructor_type.self)
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    @IBOutlet var labelResultMomID: UILabel!
    @IBOutlet var labelResultChildID: UILabel!
    @IBOutlet var viewActivityIndicator: UIView!
    var stringDeleteContacts = NSString()
    @IBOutlet var labelIdPK: UILabel!
    @IBOutlet var labelID: UILabel!
    @IBOutlet var labelname: UILabel!
    @IBOutlet var labelEmail: UILabel!
    @IBOutlet var labelMessage: UILabel!
    @IBOutlet var labelDeleteContact: UILabel!
    var arrayAllContactsSub = NSMutableArray()
    var sDuplicateNameString = NSString()
    var bDuplicateName = Bool()
    var bEmailValidation = Bool()
    var sNameConcat = String()
    var sGroupName = NSString()
    var sCellButtonPlay = NSString()
    var sDBID = NSString()
    var sResultMomid = NSString()
    var sResultChildID = NSString()
    var arrayResultTable = NSMutableArray()
    
    // AF
    var returnResponse = NSDictionary()
    var isPostMethod = Bool()
    var parameterList = NSDictionary()
    var sUrl = NSString()
    var imageUpload = UIImage()
    var bIsImage:Bool = false
    // AF--
    let arrayMailvalues = NSMutableArray()
    var bShuffle = Int()
    var arrayAllContacts = NSMutableArray()
    var arrayContscts = NSMutableArray()
    var arrayShuffledContacts = NSMutableArray()
    var newarry = NSArray()
    var  arraySelectedContacts = NSMutableArray()
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    @IBOutlet var viewEditMail: UIView!
    @IBOutlet var textName: UITextField!
    @IBOutlet var textEmailID: UITextField!
    @IBOutlet var buttonNext: UIButton!
    @IBOutlet var tableViewContacts: UITableView!
    var nRowReplace = Int()
    var tap = UITapGestureRecognizer()
    @IBOutlet var buttonAddContacts: UIButton!
    @IBOutlet var buttonAddEmail: UIButton!
    @IBOutlet var buttonCancel: UIButton!
    var date = NSDate()
    let calendar = NSCalendar.current
    let dateFormatter = DateFormatter()
    var nTime = Int()



    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        

        let calendar = NSCalendar.current
        nTime = calendar.timeZone.secondsFromGMT(for: date as Date)
        
        
        let components = calendar.component(.year, from: date as Date)
        let year =  components
        print(year)
         
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        date = NSDate(timeIntervalSinceNow: TimeInterval(nTime))

        let startDateStr = NSString(format: "01/11/%d 00:00:00",year)
        var startDate = dateFormatter.date(from: startDateStr as String)!
        
        let endDateStr = NSString(format: "07/11/%d 00:00:00",year)
        var endDate = dateFormatter.date(from: endDateStr as String)!
        
        startDate = startDate.addingTimeInterval(TimeInterval(nTime))
        endDate = endDate.addingTimeInterval(TimeInterval(nTime))


        print(startDate)
        print(endDate)
        print(date)
        
            // use a function name that matches the convention...
        if self.date(date as Date, isBetweenDate: startDate, andDate: endDate) {
                print("@@@@@YES")
             
        }
        else
        {
            buttonAddEmail.isHidden = true
            buttonAddContacts.isHidden = true

        }
      
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "dd/MM/yyyy"

        let endDateStr1 = NSString(format: "25/10/%d",year)
        let endDate1 = dateFormatter1.date(from: endDateStr1 as String)!
        print(endDate1)
        
        if self.date(endDate1 as Date, isBetweenDate: startDate, andDate: endDate) {
            print("@@@@@YES")
            
        }
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd/MM/yyyy"
        
        let endDateStr2 = NSString(format: "25/12/%d",year)
        let endDate2 = dateFormatter2.date(from: endDateStr2 as String)!
        print(endDate2)
        
        
        if self.date(endDate2 as Date, isBetweenDate: startDate, andDate: endDate) {
            print("@@@@@YES")
            
        }

        
        buttonCancel.isHidden = true
        buttonCancel.layer.frame.origin.x = buttonNext.layer.frame.origin.x
        buttonCancel.layer.frame.origin.y = buttonNext.layer.frame.origin.y
        buttonCancel.layer.frame.size.width = buttonNext.layer.frame.size.width
        buttonCancel.layer.frame.size.height = buttonNext.layer.frame.size.height
        textName.attributedPlaceholder = NSAttributedString(string: " Name", attributes: [NSForegroundColorAttributeName: UIColor.white])
        textEmailID.attributedPlaceholder = NSAttributedString(string: " Email-ID", attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        selectFromResultDB()
        if self.date(date as Date, isBetweenDate: startDate, andDate: endDate) {
            
            if arrayResultTable.count > 0
            {
                sResultChildID = "Play again"
                textName.isEnabled = false
                buttonAddContacts.isHidden = true
                buttonAddEmail.isHidden = true
                buttonNext.isHidden = true
            }else
            {
                sResultChildID = "Play"
            }
        }else
        {
            if arrayResultTable.count > 0
            {
                sResultChildID = "Result"
                textName.isEnabled = false
                tableViewContacts.allowsSelection = false
                buttonAddContacts.isHidden = true
                buttonAddEmail.isHidden = true
                buttonNext.isHidden = true
            }
        }

        
        labelID.text = sDBID as String
        nRowReplace = -1
        bEmailValidation = false
        viewActivityIndicator.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tableViewContacts.dataSource = self
        tableViewContacts.delegate = self
        self.tableViewContacts.register(UINib(nibName: "ContactsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "Identifier")
        tableViewContacts.isHidden = true
        print(arrayAllContacts)
        print(arrayAllContacts)
        viewEditMail.isHidden = true
        
        selectFromDB()
        tableViewContacts.isHidden = true
        if arrayAllContacts.count > 0
        {
            tableViewContacts.reloadData()
            tableViewContacts.isHidden = false
        }
    }
  
    func date(_ date: Date, isBetweenDate beginDate: Date, andDate endDate: Date) -> Bool {
        if date.compare(beginDate) == ComparisonResult.orderedAscending {
            return false
        }
        if date.compare(endDate) == ComparisonResult.orderedDescending {
            return false
        }
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if string.characters.count == 0
        {
            return true
        }
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        switch textField {
        case textName:
            return prospectiveText.doesNotContainCharactersIn("!@#$%^&*()_+=-<>?/:;'|[]{}") &&
                prospectiveText.characters.count <= 30

        case textEmailID:
            return prospectiveText.doesNotContainCharactersIn("") &&
                prospectiveText.characters.count <= 70
        default:
            return true
        }
    }
    func dismissKeyboard()
    {
        view.removeGestureRecognizer(tap)
        textEmailID .resignFirstResponder()
        textName .resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        textEmailID .resignFirstResponder()
        textName .resignFirstResponder()
        
        return false
    }

    // Contacts Adding Actions
    @IBAction func onTouchShowMeContactsButton(_ sender: AnyObject)
    {
        bDuplicateName = false
            let contactPickerScene = EPContactsPicker(delegate: self, multiSelection:true, subtitleCellType: SubtitleCellValue.email)
            let navigationController = UINavigationController(rootViewController: contactPickerScene)
            self.present(navigationController, animated: true, completion: nil)
        
    }
    @IBAction func buttonAddfromEmail(_ sender: AnyObject)
    {
            buttonCancel.isHidden = true
            view.addGestureRecognizer(tap)
            textEmailID.text = ""
            textName.text = ""
            viewEditMail.isHidden = false
            textName.isEnabled = true
            buttonNext.isEnabled = true
            nRowReplace = -1
    }
    
    @IBAction func buttonEditCancel(_ sender: AnyObject)
    {
        textName.text = ""
        textEmailID.text = ""
        buttonCancel.isHidden = true
        viewEditMail.isHidden = true
        nRowReplace = -1
    }
    @IBAction func buttonDoneEditContacts(_ sender: AnyObject)
    {
        bDuplicateName = false
        textEmailID .resignFirstResponder()
        textName .resignFirstResponder()
        if nRowReplace != -1
        {
            if (self.validateMail(textEmail: textEmailID.text!) == false)
            {
                let alertController = UIAlertController(title: "Alert", message: "Please Enter Valid Mail Id", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else if textName.text == ""
            {
                let alertController = UIAlertController(title: "Alert", message: "Please Enter Valid Name", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                sDuplicateNameString = NSString(format: "%@", textName.text!)
                sDuplicateNameString = sDuplicateNameString.lowercased as NSString
                
                if ((arrayAllContacts.object(at: nRowReplace) as AnyObject).value(forKey: "Name")as! String) == textName.text! as String
                {
                    
                }
                else
                {
                    validateContactName()
                }
                
                if bDuplicateName == true
                {
                    let alertController = UIAlertController(title: "Sorry", message: "This Contact name already exist", preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                        print("OK")
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                else
                {
                    UpdateToDB()
                    selectFromDB()
                    textName.text = ""
                    textEmailID.text = ""
                    tableViewContacts.isHidden = false
                    viewEditMail.isHidden = true
                    tableViewContacts.reloadData()
                }
            }
        }
        else
        {
            if (textName.text == "" && textEmailID.text == "")
            {
                viewEditMail.isHidden = true
            }
            else if (self.validateMail(textEmail: textEmailID.text!) == false)
            {
                let alertController = UIAlertController(title: "Alert", message: "Please Enter Valid Mail Id", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else if textName.text == ""
            {
                let alertController = UIAlertController(title: "Alert", message: "Please Enter Valid Name", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                sDuplicateNameString = NSString(format: "%@", textName.text!)
                validateContactName()
                if bDuplicateName == true
                {
                    let alertController = UIAlertController(title: "Sorry", message: "This Contact name already exist", preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                        print("OK")
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                else
                {
                    insertToDB()
                    selectFromDB()
                    textName.text = ""
                    textEmailID.text = ""
                    tableViewContacts.isHidden = false
                    tableViewContacts.reloadData()
                    nRowReplace = -1
                    viewEditMail.isHidden = true
                }
            }
        }
    }
    @IBAction func buttonNextEditContacts(_ sender: AnyObject)
    {
        bDuplicateName = false
            if (self.validateMail(textEmail: textEmailID.text!) == false)
            {
                let alertController = UIAlertController(title: "Alert", message: "Please Enter Valid Mail Id", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else if textName.text == ""
            {
                let alertController = UIAlertController(title: "Alert", message: "Please Enter Valid Name", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                sDuplicateNameString = NSString(format: "%@", textName.text!)
                validateContactName()
                if bDuplicateName == true
                {
                    let alertController = UIAlertController(title: "Sorry", message: "This Contact name already exist", preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                        print("OK")
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }else
                {
                    insertToDB()
                    selectFromDB()
                    textName.text = ""
                    textEmailID.text = ""
                    tableViewContacts.isHidden = false
                    tableViewContacts.reloadData()
                }
            }
    }
    //MARK: EPContactsPicker delegates
    func epContactPicker(_: EPContactsPicker, didContactFetchFailed error : NSError)
    {
        print("Failed with error \(error.description)")
    }
    func epContactPicker(_: EPContactsPicker, didSelectContact contact : EPContact)
    {
        print("Contact \(contact.displayName()) has been selected")
    }
    func epContactPicker(_: EPContactsPicker, didCancel error : NSError)
    {
        print("User canceled the selection");
    }
    
    func epContactPicker(_: EPContactsPicker, didSelectMultipleContacts contacts: [EPContact]) {

        print(arrayAllContacts.count)
        arrayAllContactsSub.removeAllObjects()
        var set = NSSet()
        for contact in contacts
        {
            let parameter = NSMutableDictionary()
            var sEmail = NSString()
            let emailCount = contact.emails.count
            if emailCount > 0
            {
                sEmail = contact.emails[0].email as NSString
            }
            else
            {
                sEmail = "No Email-ID Found"
            }
            if emailCount > 0{
                let strEmail:String = sEmail.replacingOccurrences(of: "-", with: "")
                let strEmail1:String = strEmail.replacingOccurrences(of: " ", with: "")
                parameter.setValue(strEmail1, forKey: "Email")
            }else{
                parameter.setValue(sEmail, forKey: "Email")
            }
            parameter.setValue(contact.displayName(), forKey: "Name")
            sDuplicateNameString = NSString(format: "%@", contact.displayName())
            if (arrayAllContacts.count > 0)
                {
                    validateContactName()
                    if bDuplicateName == true
                    {
                        break
                    }
                }
            print(sDuplicateNameString)
            arrayAllContactsSub.add(parameter)
            set = NSSet(array: arrayAllContactsSub as [AnyObject] )
        }
        print(contacts.count)
        print(set)
        if contacts.count > 0
        {
            if bDuplicateName == false
            {
                arrayAllContactsSub.removeAllObjects()
                arrayAllContactsSub = NSMutableArray(array: set.allObjects)
                print(arrayAllContactsSub)

                for i in 0...arrayAllContactsSub.count-1
                {
                    print(arrayAllContactsSub.object(at: i))
                    textName.text = NSString(format: "%@", ((arrayAllContactsSub.object(at: i) as AnyObject).value(forKey: "Name") as? String)!) as String
                    textEmailID.text = NSString(format: "%@", ((arrayAllContactsSub.object(at: i) as AnyObject).value(forKey: "Email") as? String)!)as String
                        insertToDB()
                        selectFromDB()
                }
            }
            else
            {
                if bDuplicateName == true
                {
                    let alertController = UIAlertController(title: "Sorry", message: "This Contact name already exist", preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                        print("OK")
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            if arrayAllContacts.count > 0
            {
                tableViewContacts.isHidden = false
                tableViewContacts.reloadData()
            }
        }
    }

    func validateContactName()
    {
        if arrayAllContacts.count > 0
        {
            for i in 0...arrayAllContacts.count-1
            {
                print(sDuplicateNameString)
                let trimmedDuplicateString = sDuplicateNameString.trimmingCharacters(in: .whitespaces)

                var sCheckString:NSString = NSString(format: "%@", (((arrayAllContacts.object(at: i)) as AnyObject).value(forKey: "Name") as? String)!)
                
                sCheckString = sCheckString.lowercased as NSString
                let trimmedString = sCheckString.trimmingCharacters(in: .whitespaces)
             //   let trimmed = sCheckString.trimmingCharacters(in: .whitespacesAndNewlines)

                print(trimmedString)

                
                if ((trimmedDuplicateString as String) == (trimmedString as String))
                {
                    bDuplicateName = true
                    break
                }
            }
        }
    }
    //MARK: - Tableview Delegate Methods -
    func numberOfSections(in tableView: UITableView) -> Int
    {
         return arrayAllContacts.count + 1
    }
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let viewHeader = UIView()
        viewHeader.backgroundColor = UIColor.clear
        return viewHeader
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if arrayAllContacts.count == (indexPath as NSIndexPath).section
         {
         
            let lastCell = tableView.dequeueReusableCell(withIdentifier: "Shuffle") as! ShuffleTableViewCell!
            [lastCell?.buttonShuffle.setTitle(sResultChildID as String, for: .normal)]
            [lastCell?.buttonShuffle.addTarget(self, action:#selector(self.buttonShuffle), for:UIControlEvents.touchUpInside)]
            lastCell? .addSubview((lastCell?.buttonShuffle)!)
            lastCell?.selectionStyle = UITableViewCellSelectionStyle.none
            let components = calendar.component(.year, from: date as Date)
            let year =  components
            print(year)
            
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
            date = NSDate(timeIntervalSinceNow: TimeInterval(nTime))
            
            let startDateStr = NSString(format: "01/11/%d 00:00:00",year)
            var startDate = dateFormatter.date(from: startDateStr as String)!
            
            let endDateStr = NSString(format: "07/11/%d 00:00:00",year)
            var endDate = dateFormatter.date(from: endDateStr as String)!
            
            startDate = startDate.addingTimeInterval(TimeInterval(nTime))
            endDate = endDate.addingTimeInterval(TimeInterval(nTime))

            let endDateStr1 = NSString(format: "25/12/%d 00:00:00",year)
            
            var endDate1 = dateFormatter.date(from: endDateStr1 as String)!
            endDate1 = endDate1.addingTimeInterval(TimeInterval(nTime))

            
            // use a function name that matches the convention...
            if self.date(date as Date, isBetweenDate: startDate, andDate: endDate)
            {
                

            }else
            {
                if arrayResultTable.count > 0
                {
                    
                }else
                {
                    lastCell?.buttonShuffle.isHidden = true
                }
            }
            return lastCell!
         }
         else
         {
            var sName = NSString()
            var sMail = NSString()
            var myMutableStringName = NSMutableAttributedString()
            var myMutableStringMail = NSMutableAttributedString()
            let cell = tableView.dequeueReusableCell(withIdentifier: "Identifier") as! ContactsTableViewCell!
            cell?.backgroundColor = UIColor.clear
            sName = NSString(format: "Name : %@", ((arrayAllContacts.object(at: (indexPath as NSIndexPath).section) as AnyObject).value(forKey: "Name") as? String)!)
            sMail = NSString(format: "Email : %@", ((arrayAllContacts.object(at: (indexPath as NSIndexPath).section) as AnyObject).value(forKey: "Email") as? String)!)
            myMutableStringName = NSMutableAttributedString(
            string: sName as String,
            attributes: [NSFontAttributeName:UIFont(
                name: "Georgia",
                size: 16.0)!])
            myMutableStringMail = NSMutableAttributedString(
            string: sMail as String,
            attributes: [NSFontAttributeName:UIFont(
                name: "Georgia",
                size: 16.0)!])
            myMutableStringName.addAttribute(NSFontAttributeName,
                                         value: UIFont(
                                            name: "Georgia-Bold",
                                            size: 16.0)!,
                                         range: NSRange(
                                            location: 0,
                                            length: 5))
            myMutableStringMail.addAttribute(NSFontAttributeName,
                                         value: UIFont(
                                            name: "Georgia-Bold",
                                            size: 16.0)!,
                                         range: NSRange(
                                            location: 0,
                                            length: 5))
            cell?.labelMailId.attributedText = myMutableStringMail
            cell?.labelName.attributedText = myMutableStringName
            print((indexPath as NSIndexPath).section)
            return cell!
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if arrayAllContacts.count != indexPath.section
        {
            let cell = tableView.cellForRow(at: indexPath)
            cell!.contentView.layer.borderColor = UIColor.red.cgColor
            textName.text = NSString(format: "%@", ((arrayAllContacts.object(at: (indexPath as NSIndexPath).section) as AnyObject).value(forKey: "Name") as? String)!) as String
            stringDeleteContacts = NSString(format: "%@", ((arrayAllContacts.object(at: (indexPath as NSIndexPath).section) as AnyObject).value(forKey: "Name") as? String)!)
            
            textEmailID.text = NSString(format: "%@", ((arrayAllContacts.object(at: (indexPath as NSIndexPath).section) as AnyObject).value(forKey: "Email") as? String)!)as String
            if textEmailID.text == "No Email-ID Found"
            {
                textEmailID.text = ""
            }
            else
            {
                textEmailID.text = NSString(format: "%@", ((arrayAllContacts.object(at: (indexPath as NSIndexPath).section) as AnyObject).value(forKey: "Email") as? String)!)as String
            }
            labelIdPK.text = NSString(format: "%@", ((arrayAllContacts.object(at: (indexPath as NSIndexPath).section) as AnyObject).value(forKey: "PrimaryKey") as? String)!)as String
            buttonCancel.isHidden = false
            buttonNext.isEnabled = false
            viewEditMail.isHidden = false
//            textName.isEnabled = true
            nRowReplace = (indexPath as NSIndexPath).section
            view.addGestureRecognizer(tap)

        }
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if arrayResultTable.count > 0
        {
        }
        else
        {
            if arrayAllContacts.count != (indexPath as NSIndexPath).section
            {
                if editingStyle == .delete
                {
                    print(((indexPath as NSIndexPath).section))
                    textName.text = NSString(format: "%@", ((arrayAllContacts.object(at: (indexPath as NSIndexPath).section) as AnyObject).value(forKey: "PrimaryKey") as? String)!) as String
                    deleteFromDB()
                    selectFromDB()
                    
                    print(arrayAllContacts)
                    if arrayAllContacts.count == 0
                    {
                        tableViewContacts.isHidden = true
                    }
                    
                    self.tableViewContacts.reloadData()
                    print(arrayAllContacts)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if arrayResultTable.count > 0
        {
            return UITableViewCellEditingStyle.none
        }
        else
        {
            if arrayAllContacts.count != (indexPath as NSIndexPath).section
            {
                return UITableViewCellEditingStyle.delete
            }
            return UITableViewCellEditingStyle.none
        }
    }

    @IBAction func buttonShuffle(_ sender : UIButton)
    {
        let components = calendar.component(.year, from: date as Date)
        let year =  components
        print(year)
        
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        date = NSDate(timeIntervalSinceNow: TimeInterval(nTime))
        
        let startDateStr = NSString(format: "01/11/%d 00:00:00",year)
        var startDate = dateFormatter.date(from: startDateStr as String)!
        
        let endDateStr = NSString(format: "07/11/%d 00:00:00",year)
        var endDate = dateFormatter.date(from: endDateStr as String)!
        
        startDate = startDate.addingTimeInterval(TimeInterval(nTime))
        endDate = endDate.addingTimeInterval(TimeInterval(nTime))
        
        let endDateStr1 = NSString(format: "25/12/%d 00:00:00",year)
        
        var endDate1 = dateFormatter.date(from: endDateStr1 as String)!
        endDate1 = endDate1.addingTimeInterval(TimeInterval(nTime))
        


        if self.date(date as Date, isBetweenDate: startDate, andDate: endDate) {
            
            if arrayAllContacts.count <= 1
            {
                let alertController = UIAlertController(title: "Alert", message: "Please add more than 1 participant", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }else
            {
                viewActivityIndicator.isHidden = false
                if arrayResultTable.count > 0
                {
                    let alertController1 = UIAlertController(title: "Alert", message: "Play again will re-send email to all the participants", preferredStyle: UIAlertControllerStyle.alert)
                    let cancelAction1 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                        self.viewActivityIndicator.isHidden = true
                    }
                    alertController1.addAction(cancelAction1)

                    let OKAction1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        self.playAgain()
                    }
                    alertController1.addAction(OKAction1)
                    self.present(alertController1, animated: true, completion:nil)
                }
                else
                {
                    shuffle()
                    if bEmailValidation == false
                    {
                        let alertController1 = UIAlertController(title: "Alert", message: "Once played you cannot add or delete a participant from the group", preferredStyle: UIAlertControllerStyle.alert)
                        let cancelAction1 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                            self.viewActivityIndicator.isHidden = true
                        }
                        alertController1.addAction(cancelAction1)
                        
                        let OKAction1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.navigate()
                        }
                        alertController1.addAction(OKAction1)
                        self.present(alertController1, animated: true, completion:nil)
                    }
                    if bEmailValidation == true
                    {
                        viewActivityIndicator.isHidden = true
                        sNameConcat = String(sNameConcat.characters.dropLast())
                        let alertController = UIAlertController(title: "Alert", message: "Please Enter valid mail-id for \(sNameConcat)", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                            print("OK")
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            
            if arrayResultTable.count > 0
            {
                showResult()
            }
        }
        
        
    }
    func showResult()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        nextViewController.sDBID = sDBID
        
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    func shuffle()
    {
        bEmailValidation = false
        var concatString = ""
        for i in 0...arrayAllContacts.count-1
        {
            let sName = NSString(format: "%@", ((arrayAllContacts.object(at: i) as AnyObject).value(forKey: "Name") as? String)!)
            let sEmail = NSString(format: "%@", ((arrayAllContacts.object(at: i) as AnyObject).value(forKey: "Email") as? String)!)
            if (validateMail(textEmail: sEmail as String) == false)
            {
                bEmailValidation = true
                concatString += "\(sName),"
                sNameConcat = String(format: "%@", concatString)
                print(sNameConcat)
            }
        }
        if bEmailValidation == false
        {
            let arrayContactsInsert = NSMutableArray()
            print(arrayContactsInsert)
            for i in 0...arrayAllContacts.count-1
            {
                let parameter = NSMutableDictionary()
                let sName = NSString(format: "%@", ((arrayAllContacts.object(at: i) as AnyObject).value(forKey: "Name") as? String)!)
                let sEmail = NSString(format: "%@", ((arrayAllContacts.object(at: i) as AnyObject).value(forKey: "Email") as? String)!)
                let sPK = NSString(format: "%@", ((arrayAllContacts.object(at: i) as AnyObject).value(forKey: "PrimaryKey") as? String)!)
                
                parameter.setValue(i+1, forKey: "int")
                parameter.setValue(sName, forKey: "Name")
                parameter.setValue(sEmail, forKey: "Email")
                parameter.setValue(sPK, forKey: "PrimaryKey")
                arrayContactsInsert.add(parameter)
                print(arrayContactsInsert)
            }
            arrayAllContacts.removeAllObjects()
            arrayAllContacts = arrayContactsInsert
            print(arrayAllContacts)
            
            if #available(iOS 9.0, *) {
                let shuffledBalls = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: arrayAllContacts as [AnyObject])
                arrayShuffledContacts.removeAllObjects()
                arrayShuffledContacts.addObjects(from: shuffledBalls)
            } else {
                // Fallback on earlier versions
            }

            for i in 0...arrayShuffledContacts.count-1
            {
                if (((arrayShuffledContacts.object(at: i) as AnyObject).value(forKey: "int") as? Int) == ((arrayAllContacts.object(at: i) as AnyObject).value(forKey: "int") as! Int))
                {
                    print("same")
                    shuffle()
                    break
                }
            }
            print(arrayShuffledContacts)
        }
    }
    func playAgain()
    {
        arrayShuffledContacts.removeAllObjects()
        print(arrayResultTable.count)
        print(arrayAllContacts.count)

        for i in 0...arrayResultTable.count-1
        {
            let parameter = NSMutableDictionary()
            let nShuffledName:NSString = NSString(format:"%@",((arrayResultTable.object(at: i) as AnyObject).value(forKey: "child_id") as? String)!)
            var nChildValue:Int = Int(nShuffledName.intValue)
            nChildValue = nChildValue - 1
            print(nChildValue)
            var stringShuffledName = NSString()
            stringShuffledName = NSString(format: "%@", ((arrayAllContacts.object(at: nChildValue as Int ) as AnyObject).value(forKey: "Name") as? String)!)
            parameter.setValue(stringShuffledName, forKey: "Name")
            arrayShuffledContacts.add(parameter)
            print(arrayShuffledContacts.count)

        }
        print(arrayShuffledContacts)
        navigate()
    }
    func navigate()
    {
        if arrayResultTable.count < 1
        {
            print(arrayAllContacts)
        }
        for i in 0...arrayAllContacts.count-1
        {
            let dictMailvalues = NSMutableDictionary()
            var stringMessageValues = NSString()
            var stringMail = NSString()
            stringMessageValues = NSString(format: "Dear %@,\n\nYour Chris child name is %@ \n\nYou are part of the Chris mom Chris child game for this Christmas \n\nGroup Name: %@ \n\n Participants : \n ", ((arrayAllContacts.object(at: i) as AnyObject).value(forKey: "Name") as? String)!,((arrayShuffledContacts.object(at: i) as AnyObject).value(forKey: "Name") as? String)!,sGroupName)
            print(stringMessageValues)
            
            for j in 0...arrayAllContacts.count-1
            {
                let stringName:NSString =  NSString(format:"%@",((arrayAllContacts.object(at: j) as AnyObject).value(forKey: "Name") as? String)!)
                print(stringName)
                let stringAppend:NSString = NSString(format: "\n%d. %@ ", j+1, stringName)
                stringMessageValues = "\(stringMessageValues) \(stringAppend)" as NSString

                print(stringMessageValues)
            }
            let stringAppendMailStatic:NSString = "\n\nWe wish you and your loved ones a Merry Christmas and a happy New Year.\n\nDon't forget to get your Chris child a gift :)\n\nBest regards from,\nChris mom Chrid child App\nYou can download it from App store ( link :                         https://itunes.apple.com/app/id1172749561)"
            stringMessageValues = "\(stringMessageValues) \(stringAppendMailStatic)"as NSString
            print(stringMessageValues)
            stringMail = NSString(format: "%@", ((arrayAllContacts.object(at: i) as AnyObject).value(forKey: "Email") as? String)!)
            dictMailvalues.setValue(stringMessageValues, forKey: "message")
            dictMailvalues.setValue(stringMail, forKey: "email")
            arrayMailvalues.add(dictMailvalues)
            print(arrayMailvalues)
        }
        print(arrayMailvalues)
        let parametermail = NSMutableDictionary()
        var string = NSString()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: arrayMailvalues, options: JSONSerialization.WritingOptions.prettyPrinted)
            print(jsonData)
            string = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)!
            print(string)
            parametermail.setValue(string, forKey: "jsonrequest")
            print(jsonData)
            
        }catch let error as NSError
        {
            print(error.description)
        }
        print(parametermail)
        callWebservicePostMethod(params: parametermail, stringUrl: NSString(format: "%@%@",ApiClass().ApiHost,ApiClass().ArrayUrl))
    }
    // Email Validation
    func validateMail(textEmail: String) -> Bool
    {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: textEmail)
    }
    
    var player: AVAudioPlayer?
    func playSound() {
        let url = Bundle.main.url(forResource: "ChrisMa", withExtension: "mpeg")!        
        do {
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            player.prepareToPlay()
            player.volume = 1.0
            player.play()
        } catch let error as NSError {
            print(error.description)
        }
    }
    // AfNetworking
    func callWebservicePostMethod(params:NSDictionary, stringUrl:NSString)
    {
        viewActivityIndicator.isHidden = false
        let manager = AFHTTPSessionManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/plain", "text/html", "application/json", "audio/wav", "application/octest-stream") as? Set<String>
        manager.post(stringUrl as String, parameters: params, progress: nil, success: { (operation, responseObject) -> Void in
            print(responseObject as Any)
            
            if self.arrayResultTable.count > 0
            {
            }
            else
            {
                for i in 0...self.arrayAllContacts.count-1
                {
                    self.sResultMomid = NSString(format:"%d",((self.arrayAllContacts.object(at: i) as AnyObject).value(forKey: "int") as? Int)!)
                    self.sResultChildID = NSString(format: "%d", ((self.arrayShuffledContacts.object(at: i) as AnyObject).value(forKey: "int") as? Int)!)
                    print(self.sResultMomid)
                    print(self.sResultChildID)
                    self.insertResultToDB()
                }

            }
           
            self.returnResponse = responseObject as! NSDictionary
            self.viewActivityIndicator.isHidden = true
            self.playStatus()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ResultViewController") as! ResultViewController
            
                    self.navigationController?.pushViewController(nextViewController, animated: true)

            self.dismiss(animated: false, completion: nil)
            }, failure: { (operation, error) -> Void in
                print(error.localizedDescription)
                self.viewActivityIndicator.isHidden = true

                let alertController = UIAlertController(title: "Connection Failure", message: "Check your internet Settings", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    print("OK")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
        })
    }
    
    @IBAction func buttonBack(_ sender: AnyObject)
    {
        if viewEditMail.isHidden == false
        {
            view.removeGestureRecognizer(tap)
            viewEditMail.isHidden = true
        }
        else if viewEditMail.isHidden == true
        {
            _ = navigationController?.popViewController(animated: true)
            
        }
    }
    func insertToDB()
    {
        arrayAllContacts.removeAllObjects()

        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ChrisMomChrisSon.sqlite")
        
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        // Insert Values to DB
        var statement: OpaquePointer? = nil
        
        
        if sqlite3_prepare_v2(db, "insert into GroupContacts (gid,cname,email) values (?,?,?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing insert: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 1, labelID.text, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 2, textName.text, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 3, textEmailID.text, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_step(statement) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure inserting foo: \(errmsg)")
        }
    }
    func selectFromDB()
    {
        arrayAllContacts.removeAllObjects()
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ChrisMomChrisSon.sqlite")
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        // Select Values From DB
        var statement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, "select * from GroupContacts where gid= (?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 1, labelID.text, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        while sqlite3_step(statement) == SQLITE_ROW
        {
            let dTempDictionary = NSMutableDictionary()
            let name = sqlite3_column_text(statement, 1)
            if name != nil
            {
                let nameString = String(cString:(name!))
                print(nameString)
                dTempDictionary.setValue(nameString, forKey: "Name")
            }
            let email = sqlite3_column_text(statement, 2)
            if email != nil
            {
                let emailString = String(cString:(email!))
                print(emailString)
                dTempDictionary.setValue(emailString, forKey: "Email")
            }
            let idPrimaryKey = sqlite3_column_text(statement, 3)
            if idPrimaryKey != nil
            {
                let idPrimaryKey = String(cString:(idPrimaryKey!))
                print(idPrimaryKey)
                dTempDictionary.setValue(idPrimaryKey, forKey: "PrimaryKey")
            }
            arrayAllContacts.add(dTempDictionary)
            print(arrayAllContacts)
            var set = NSSet()
            set = NSSet(array: arrayAllContacts as [AnyObject] )
            arrayAllContacts = NSMutableArray(array: set.allObjects)
            print(arrayAllContacts)
            let   brandDescriptor = NSSortDescriptor(key: "Name", ascending: true)
            let sortDescriptors = [brandDescriptor]
            let sortedArray = (arrayAllContacts as NSMutableArray).sortedArray(using: sortDescriptors)
            print(sortedArray)
            arrayAllContacts.removeAllObjects()
            arrayAllContacts.addObjects(from: sortedArray)
            
        }
        print(arrayAllContacts.count)

        insertGroupCount()
        tableViewContacts.reloadData()
    }
    
    func deleteFromDB()
    {
        arrayAllContacts.removeAllObjects()

        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ChrisMomChrisSon.sqlite")
        
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        // Insert Values to DB
        var statement: OpaquePointer? = nil

        if sqlite3_prepare_v2(db, "DELETE FROM GroupContacts where cid= (?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing Delete: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 1, textName.text, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_step(statement) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure deleting data: \(errmsg)")
        }
    }
    func UpdateToDB()
    {
        arrayAllContacts.removeAllObjects()
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ChrisMomChrisSon.sqlite")
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        var statement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, "update GroupContacts set email = ? where cid = (?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing insert: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 1, textEmailID.text, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 2, labelIdPK.text, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_step(statement) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure inserting foo: \(errmsg)")
        }

        if sqlite3_prepare_v2(db, "update GroupContacts set cname = ? where cid = (?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing insert: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 1, textName.text, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 2, labelIdPK.text, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_step(statement) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure inserting foo: \(errmsg)")
        }


    }
    func insertGroupCount()
    {
        labelname.text = NSString(format: "%d", arrayAllContacts.count) as String
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ChrisMomChrisSon.sqlite")
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        var statement: OpaquePointer? = nil
        let cGroupID = (sDBID as NSString).utf8String
        if sqlite3_prepare_v2(db, "UPDATE Groups set gcount = (?) where gid= (?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing insert: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 1, labelname.text, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 2, cGroupID, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }

        if sqlite3_step(statement) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure inserting foo: \(errmsg)")
        }
    }
    func playStatus()
    {
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ChrisMomChrisSon.sqlite")
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        var statement: OpaquePointer? = nil
        let cGroupID = (sDBID as NSString).utf8String
        if sqlite3_prepare_v2(db, "UPDATE Groups set playStatus = (?) where gid= (?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing insert: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 1, "1", -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 2, cGroupID, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        
        if sqlite3_step(statement) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure inserting foo: \(errmsg)")
        }
    }
    func insertResultToDB()
    {
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ChrisMomChrisSon.sqlite")
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        var statement: OpaquePointer? = nil
        let cMomID = (sResultMomid as NSString).utf8String
        let cChildID = (sResultChildID as NSString).utf8String
        let cGroupID = (sDBID as NSString).utf8String
        print(cMomID as Any)
        print(cChildID as Any)
        print(cGroupID as Any)
        if sqlite3_prepare_v2(db, "insert into resultTable (gid,momid,childid) values (?,?,?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing insert: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 1, cGroupID, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 2,cMomID
, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 3, cChildID, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        if sqlite3_step(statement) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure inserting foo: \(errmsg)")
        }
    }
    func selectFromResultDB()
    {
        arrayResultTable.removeAllObjects()
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ChrisMomChrisSon.sqlite")
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        // Select Values From DB
        let cGroupID = (sDBID as NSString).utf8String
        var statement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, "select * from resultTable where gid= (?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        if sqlite3_bind_text(statement, 1, cGroupID, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding foo: \(errmsg)")
        }
        while sqlite3_step(statement) == SQLITE_ROW
        {
            let dTempDictionary = NSMutableDictionary()
            let mom_id = sqlite3_column_text(statement, 1)
            if mom_id != nil
            {
                let momIDString = String(cString:(mom_id!))
                print(momIDString)
                dTempDictionary.setValue(momIDString, forKey: "mom_id")
            }
            let childID = sqlite3_column_text(statement, 2)
            if childID != nil
            {
                let childIDString = String(cString:(childID!))
                print(childIDString)
                dTempDictionary.setValue(childIDString, forKey: "child_id")
            }
            let idPrimaryKey = sqlite3_column_text(statement, 3)
            if idPrimaryKey != nil
            {
                let idPrimaryKey = String(cString:(idPrimaryKey!))
                print(idPrimaryKey)
                dTempDictionary.setValue(idPrimaryKey, forKey: "PrimaryKey")
            }
            arrayResultTable.add(dTempDictionary)
            print(arrayResultTable)
            print(arrayAllContacts)
        }
        print(arrayResultTable)
    }
    override func viewWillAppear(_ animated: Bool) {
        selectFromDB()
        insertGroupCount()
    }
}

