import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class EditContactViewController: UIViewController
{
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    @IBOutlet var buttonBack: UIButton!
    @IBOutlet var textViewRules: UITextView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let stringRules:NSString = "FAQ:-\n\n1.When can we play this game ?\n\nYou can only play this game during the months November and December using this App. The result will be displayed on Christmas day.\n\n2. Can I edit participants details once played ?\n\nYou can only edit the email id of the participants you cannot edit the name.\n\n3. Can I add or remove participants from the group once played ?\n\nYou cannot add or remove participants once played but you can only edit their email and replay the game. If you want to add more participants you need to create a new group.\n\n4. When we will receive the Emails ?\n\nSoon after the playing the game it may take few minutes (i.e. 20 - 40 min) to receive emails.You will receive an email from info@chirsmomandchildapp.com\n\n5. What should I do if someone in the group does not receive the Email ?\n\nWait for some more hours or please check your spam folder too. Yet if some does not receive the email it may be due to wrong email id you can edit their email id and play again.  Playing again will only re send the email to same Chris mom’s. It does not shuffle the Chris child’s again.\n\n6. What will happen if I Play again ?\n\nPlay again will not change assigned Chris mom and child’s of a group it will only resend the email again to all the participants with their Chris child details. Play again is used in the case if someone participants from the group does not receive the emails. Result is fixed after the first play for each groups.\n\n7. When we will know the results ?\n\nOn Christmas day we will display the results in the App."
        let attributedText: NSMutableAttributedString = NSMutableAttributedString(string: stringRules as String)
      
      
      

        
        attributedText.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)], range: NSRange(location: 0, length: 5))
/* 1*/   attributedText.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)], range: NSRange(location: 6, length: 31))
        
        attributedText.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 16)], range: NSRange(location: 38, length: 136))
        
  /* 2*/       attributedText.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)], range: NSRange(location: 170, length: 55))

        attributedText.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 16)], range: NSRange(location: 221, length: 87))
        
    /* 3*/       attributedText.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)], range: NSRange(location: 298, length: 65))
        attributedText.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 16)], range: NSRange(location: 363, length: 177))

   /* 4*/      attributedText.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)], range: NSRange(location: 539, length: 38))
       
        attributedText.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 16)], range: NSRange(location: 578, length: 152))
      
  /* 5*/      attributedText.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)], range: NSRange(location: 731, length: 74))
        attributedText.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 16)], range: NSRange(location: 805, length: 288))
     
    /* 6*/attributedText.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)], range: NSRange(location: 1092, length: 40))
        attributedText.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 16)], range: NSRange(location: 1132, length: 314))
       
        attributedText.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)], range: NSRange(location: 1447, length: 35))
        attributedText.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 16)], range: NSRange(location: 1483, length: 57))

        attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location:0,length:1540))


//        attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location:6,length:56))
//        attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location:45,length:87))
//        attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location:137,length:62))
//
//        attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location:199,length:177))


        textViewRules.attributedText = attributedText
    
    }

    @IBAction func buttonBackSearch(_ sender: AnyObject)
    {
        _ = navigationController?.popViewController(animated: true)
    }

}
