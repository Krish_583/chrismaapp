import UIKit
import AVFoundation


class ResultViewController: UIViewController  {
   
    @IBOutlet weak var labelResult: UILabel!
    @IBOutlet var tableViewResult: UITableView!
    let arrayMailvalues = NSMutableArray()
    var arrayContacts = NSMutableArray()
    var arrayShuffledContacts = NSMutableArray()
    let parameter = NSMutableDictionary()
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    
    override func viewDidLoad()
    {
        playSound()
        let sResultText:NSString = "Thanks for using this App to celebrate Christmas.\n\nEmail is secretly sent to all the Chris mom’s.\n\nWe will disclose the secret on Christmas day.\n\nFor any queries please check the FAQ."
        labelResult.text = sResultText as String
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        player?.stop()
    }
    var player: AVAudioPlayer?
    
    func playSound() {
        let url = Bundle.main.url(forResource: "ChrisMa", withExtension: "mp3")!
        do {
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            player.prepareToPlay()
            player.volume = 0.2
            player.play()
        } catch let error as NSError {
            print(error.description)
        }
    }

    @IBAction func buttonBackSearch(_ sender: AnyObject)
    {
    //  _ = navigationController?.popViewController(animated: true)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:"LoginPage") as! LoginPage
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
 
}
