//
//  ResultPageTableViewCell.swift
//  CMomCChild
//
//  Created by amerr on 05/11/16.
//  Copyright © 2016 Jeyavijay. All rights reserved.
//

import UIKit

class ResultPageTableViewCell: UITableViewCell {

    @IBOutlet weak var labelChrisMom: UILabel!
    
    @IBOutlet weak var labelChrisChild: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
